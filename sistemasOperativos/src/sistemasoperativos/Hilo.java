/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sistemasoperativos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CHARL
 */
public class Hilo extends Thread {

    javax.swing.JLabel jLabel1;
    private int contador = 0;

    public Hilo(String nombre) {
        super(nombre);
    }

    @Override
    public void run() {
        while (true) {
            jLabel1.setText(getName() + contador++);
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    public void recibeJLabel1(javax.swing.JLabel jLabel1){
        this.jLabel1 = jLabel1;
    }
}
